﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace QuanLyQuanCafe.DAO
{
    public class DataProvider
    {
        private static DataProvider instance;//ctrl+R+E

        public static DataProvider Instance
        {
            get { if (instance == null) instance = new DataProvider(); 
                return DataProvider.instance; }
            private set { DataProvider.instance = value; }
            //chir trong class nay moi duoc chinh sua con ben ngoai khong doc dung vao
        }
        private DataProvider() { }
        private string ConnectionSTR = "Data Source=.\\sqlexpress;Initial Catalog=QuanLyQuanCaFe;Integrated Security=True";
        public DataTable ExecuteQuery(string query,object[] Parameter=null)//string query
        {
            // khởi tạo datatable để lưu dữ liệu
            DataTable datatable = new DataTable();
            //tạo kết nối từ client đến server
            //Using :cho dù có chuyện gì xảy ra thì khi thực thi hết các lệnh trong khối lệnh thì sẽ giải phóng vùng nhớ
            using (SqlConnection connection = new SqlConnection(ConnectionSTR))
            {
                //mở connection
                connection.Open();
                // thực thi câu truy vấn
                SqlCommand command = new SqlCommand(query, connection);
               if(Parameter!=null)
               {
                   string[] Listpara = query.Split(' ');
                   int i = 0;
                   foreach(string item in Listpara)
                   {
                       if(item.Contains('@'))
                       {
                           command.Parameters.AddWithValue(item, Parameter[i]);
                               i++;
                       }
                   }


               }
                // bước trung gian để lấy dữ liệu
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                //đổ dữ liệu ra datatable
                adapter.Fill(datatable);
                //đóng kết nối để tránh tình trạng nhiều nguồn dữ liệu đổ về gây ra lỗi
                connection.Close();

            }
   
           return datatable;
        }
       //trả về số dòng thành công
        public int ExecuteNoneQuery(string query, object[] Parameter = null)//string query
        {
            int data = 0;
            //tạo kết nối từ client đến server
            //Using :cho dù có chuyện gì xảy ra thì khi thực thi hết các lệnh trong khối lệnh thì sẽ giải phóng vùng nhớ
            using (SqlConnection connection = new SqlConnection(ConnectionSTR))
            {
                //mở connection
                connection.Open();
                // thực thi câu truy vấn
                SqlCommand command = new SqlCommand(query, connection);
                if (Parameter != null)
                {
                    string[] Listpara = query.Split(' ');
                    int i = 0;
                    foreach (string item in Listpara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, Parameter[i]);
                            i++;
                        }
                    }


                }
                data = command.ExecuteNonQuery();
                //đóng kết nối để tránh tình trạng nhiều nguồn dữ liệu đổ về gây ra lỗi
                connection.Close();

            }

            return data;
        }
        //đếm số lượng
        public object ExecuteScalar(string query, object[] Parameter = null)//string query
        {
            object data = 0;
            //tạo kết nối từ client đến server
            //Using :cho dù có chuyện gì xảy ra thì khi thực thi hết các lệnh trong khối lệnh thì sẽ giải phóng vùng nhớ
            using (SqlConnection connection = new SqlConnection(ConnectionSTR))
            {
                //mở connection
                connection.Open();
                // thực thi câu truy vấn
                SqlCommand command = new SqlCommand(query, connection);
                if (Parameter != null)
                {
                    string[] Listpara = query.Split(' ');
                    int i = 0;
                    foreach (string item in Listpara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, Parameter[i]);
                            i++;
                        }
                    }


                }
                data = command.ExecuteScalar();
                //đóng kết nối để tránh tình trạng nhiều nguồn dữ liệu đổ về gây ra lỗi
                connection.Close();

            }

            return data;
        }
    }

}
