﻿using QuanLyQuanCafe.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.DAO
{
    public class BillDAO
    {
        private static BillDAO instance;

        public static BillDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new BillDAO();
                return BillDAO.instance;
            }

            private set
            {
                instance = value;
            }
        }
        private BillDAO() { }
        /// <summary>
        /// thanh cong ==> billid
        /// that bai :-1
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetUncheckBillIdByTableId(int id)
        {
            string query = "select * from Bill where idTable=" + id + "and status=0";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            if (data.Rows.Count > 0)
            {
                Bill bill = new Bill(data.Rows[0]);
                return bill.Id;
            }
            return -1;
        }
        public void InsertBill(int id)
        {
            DataProvider.Instance.ExecuteNoneQuery("USP_InsertBill @idTable", new object[] { id });
        }
        public int GetMaxIdBill()
        {
            try
            {
                return (int)DataProvider.Instance.ExecuteScalar("select max(id) from bill");
            }
            catch
            {
                return 1;
            }
          
        }
        public void CheckOut(int id,int discount)
        {
            string query = "update bill set status=1 ,"+"discount="+discount +" where id="+id;
            DataProvider.Instance.ExecuteNoneQuery(query); 

        }
    }
}
