﻿using QuanLyQuanCafe.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.DAO
{
    public class MenuDao
    {
        private static MenuDao instance;

        public static MenuDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new MenuDao();
                return MenuDao.instance;
            }

            private set
            {
                MenuDao.instance = value;
            }
        }
        private MenuDao()
        {

        }
        public List<Menu> GetListMenuByTable(int id)
        {
            List<Menu> listmenu = new List<Menu>();
            string query = "USP_GetMenuInfo @id";
            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { id });
            foreach(DataRow item in result.Rows)
            {
                Menu menu = new Menu(item);
                listmenu.Add(menu);
            }
            return listmenu;
        }

    }
}
