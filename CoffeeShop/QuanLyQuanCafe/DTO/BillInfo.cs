﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.DTO
{
    public class BillInfo
    {
        private int iD;
        private int billId;
        private int foodId;
        private int count;
        public BillInfo(int id,int billid,int foodid,int count)
        {
            this.ID = id;
            this.BillId = billid;
            this.FoodId = foodid;
            this.Count = count;
        }
        public BillInfo(DataRow row)
        {
            this.ID = (int)row["id"];
            this.BillId = (int)row["idBill"];
            this.FoodId = (int)row["idFood"];
            this.Count = (int)row["count"];
        }

        public int BillId
        {
            get
            {
                return billId;
            }

            set
            {
                billId = value;
            }
        }

        public int FoodId
        {
            get
            {
                return foodId;
            }

            set
            {
                foodId = value;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }

            set
            {
                count = value;
            }
        }

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }
    }
}
