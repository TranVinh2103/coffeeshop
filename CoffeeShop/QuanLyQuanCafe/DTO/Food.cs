﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.DTO
{
   public class Food
    {
        private int id;
        private string name;
        private int categoryID;
        private float price;
        public Food(int id,string name, int categoryid,float price)
        {
            this.Id = id;
            this.Name = name;
            this.CategoryID = categoryid;
            this.Price = price;
        }
        public Food(DataRow row)
        {
            this.Id =(int)row["id"];
            this.Name = row["name"].ToString();
            this.CategoryID = (int)row["idcategory"];
            this.Price =(float)Convert.ToDouble(row["price"].ToString());

        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int CategoryID
        {
            get
            {
                return categoryID;
            }

            set
            {
                categoryID = value;
            }
        }

        public float Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
