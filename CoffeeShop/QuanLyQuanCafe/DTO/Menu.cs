﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.DTO
{
    public class Menu
    {
        public Menu() { }
        public Menu(string foodname,int count,float price, float totalprice=0)
        {
            this.Foodname = foodname;
            this.Count = count;
            this.Price = price;
            this.TotalPrice = totalprice;
        }
        public Menu(DataRow row)
        {
            this.Foodname =row["name"].ToString();
            this.Count = (int)row["count"];
            this.Price = (float)Double.Parse( row["price"].ToString()); 
            this.TotalPrice = (float)Double.Parse(row["totalprice"].ToString());
        }

        private float totalPrice;
        public float TotalPrice
        {
            get
            {
                return totalPrice;
            }

            set
            {
                totalPrice = value;
            }
        }

        private int count;
        public int Count
        {
            get
            {
                return count;
            }

            set
            {
                count = value;
            }
        }
        private string foodname;
        public string Foodname
        {
            get
            {
                return foodname;
            }

            set
            {
                foodname = value;
            }
        }
        private float price;
        public float Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }

        
    }
}
