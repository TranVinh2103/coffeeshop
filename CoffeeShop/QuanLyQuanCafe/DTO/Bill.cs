﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.DTO
{
    public class Bill
    {
        private int id;
        private DateTime? datecheckin;
        private DateTime? datecheckout;
        private int status;
        private int discount;
        public Bill() { }
        public Bill(int id,DateTime? datacheckin,DateTime? datecheckout,int status,int discount=0)
        {
            this.Id = id;
            this.Datecheckin = datecheckin;
            this.Datecheckout = datecheckout;
            this.Status = status;
            this.Discount = discount;
        }
        public Bill(DataRow row)
        {
            this.Id = (int)row["id"];
            this.Datecheckin = (DateTime?)row["datecheckin"];
            var datecheckoutTemp = row["datecheckout"];
            if(datecheckoutTemp.ToString()!="")
                this.Datecheckout = (DateTime?)datecheckoutTemp;
            this.Status = (int)row["status"];
            this.Discount = (int)row["discount"];
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public DateTime? Datecheckin
        {
            get
            {
                return datecheckin;
            }

            set
            {
                datecheckin = value;
            }
        }

        public DateTime? Datecheckout
        {
            get
            {
                return datecheckout;
            }

            set
            {
                datecheckout = value;
            }
        }

        public int Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public int Discount
        {
            get
            {
                return discount;
            }

            set
            {
                discount = value;
            }
        }
    }
}
