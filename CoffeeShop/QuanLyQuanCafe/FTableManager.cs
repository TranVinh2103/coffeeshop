﻿using QuanLyQuanCafe.DAO;
using QuanLyQuanCafe.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanCafe
{
    public partial class FTableManager : Form
    {       
        public FTableManager()
        {
            InitializeComponent();
            LoadTable();
            LoadCategory();           
        }
        #region Method
        private void LoadTable()
        {
            flpTable.Controls.Clear();
            List<Table> tableList = TableDAO.Instance.LoadTableList();
            foreach (Table item in tableList)
            {
                Button btn = new Button() {Width= TableDAO.tableWidth,Height= TableDAO.tableHeight };              
                btn.Text = item.Name + Environment.NewLine+item.Status;
                btn.Click += Btn_Click;
                btn.Tag = item;
                switch (item.Status)
                {
                    case "Có Người":
                        btn.BackColor = Color.LightPink;
                        break;
                    default:
                        btn.BackColor = Color.Aqua;
                        break;
                }

                flpTable.Controls.Add(btn);
            }
        }
       

        void showBill(int id)
        {
            lvwBill.Items.Clear();
            List<DTO.Menu> listBillInfo = MenuDao.Instance.GetListMenuByTable(id);
            float totalPrice = 0;        
            foreach(DTO.Menu item in listBillInfo)
            {               
                ListViewItem lsvItem = new ListViewItem(item.Foodname.ToString());
                lsvItem.SubItems.Add(item.Count.ToString());
                lsvItem.SubItems.Add(item.Price.ToString());
                lsvItem.SubItems.Add(item.TotalPrice.ToString());
                totalPrice += item.TotalPrice;
                lvwBill.Items.Add(lsvItem);
            }
            CultureInfo culture = new CultureInfo("vi-VN");
            
            txbTotalPrice.Text = totalPrice.ToString("c",culture);
        }
        void LoadCategory()
        {
            List<Category> listCategory = CategoryDAO.Instance.GetListCategory();
            cbxCategory.DataSource=listCategory;
            cbxCategory.DisplayMember = "Name";
        }
        void LoadFoodListByCategoryID(int id)
        {
            List<Food> listFood = FoodDAO.Instance.GetListFoodByCategoryID(id);
            cbxFood.DataSource = listFood;
            cbxFood.DisplayMember = "Name";
        }
        #endregion
        #region Events
        private void Btn_Click(object sender, EventArgs e)
        {
            int tableid = ((sender as Button ).Tag as Table).ID;
            lvwBill.Tag = (sender as Button).Tag;
            showBill(tableid);
        }


        private void đăngXuâtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void thôngTinCaNhânToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FProFile f = new FProFile();
            f.ShowDialog();
        }

        private void admindToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FAdmind f = new FAdmind();
            f.ShowDialog();
        }

        private void FTableManager_Load(object sender, EventArgs e)
        {

        }
        private void lvwBill_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void cbxCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = 0;
            ComboBox cb = sender as ComboBox;
            if (cb.SelectedItem == null)
                return;
            Category selected = cb.SelectedItem as Category;
            id = selected.Id;
            LoadFoodListByCategoryID(id);
        }
        private void btnAddFoot_Click(object sender, EventArgs e)
        {
            Table table = lvwBill.Tag as Table;
            int idBill = BillDAO.Instance.GetUncheckBillIdByTableId(table.ID);
            int idFood = (cbxFood.SelectedItem as Food).Id;
            int count = (int)numfoodCount.Value;
            if (idBill == -1)
            {
                BillDAO.Instance.InsertBill(table.ID);
                BillInfoDAO.Instance.InsertBillInfo(BillDAO.Instance.GetMaxIdBill(), idFood, count);
            }
            else
            {
                BillInfoDAO.Instance.InsertBillInfo(idBill, idFood, count);
            }
            showBill(table.ID);
            LoadTable();
        }
        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            Table table = lvwBill.Tag as Table;
            int idBill = BillDAO.Instance.GetUncheckBillIdByTableId(table.ID);
          
            double totalPrice = Convert.ToDouble(txbTotalPrice.Text.Split(',')[0]);           
            int discount = (int)numDisCount.Value;
            double disPrice = totalPrice - (totalPrice / 100) * discount;           
            if(idBill !=-1)
            {
                if (MessageBox.Show(string.Format("Bạn có chắc chắn muốn thanh toán hóa đơn cho bàn {0}\n Tổng tiền -  (Tổng tiền/100) x Giảm giá={1} - ({1}/100) x {2}= {3}" , table.Name,totalPrice,discount,disPrice), "Thông Báo", 
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    BillDAO.Instance.CheckOut(idBill,discount);
                    showBill(table.ID);
                    LoadTable();

                }
            }
        }

        #endregion


    }
}
